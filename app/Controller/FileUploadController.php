<?php

class FileUploadController extends AppController {
	public function index() {
		$this->set('title', __('File Upload Answer'));

		if($this->request->is('post')){
		    $file = $this->request->data['FileUpload']['file'];
		    
		    if($file['error'] === UPLOAD_ERR_OK) {
		        $upload_data = file_get_contents($file['tmp_name']);
		        $upload_data = str_getcsv($upload_data, "\r");
		        foreach($upload_data as $i => &$row_data){
		            $row_data = str_getcsv($row_data, ",");
		            if($i > 0){
		                $this->FileUpload->create();
		                $this->FileUpload->save(array(
		                    strtolower($upload_data[0][0]) => $row_data[0],
		                    strtolower($upload_data[0][1]) => $row_data[1],
		                ));
		            }
		        }

		        unset($i, $row_data, $upload_data);
		    }

		    unset($file);
		}

		$file_uploads = $this->FileUpload->find('all',array('order'=>array('FileUpload.created'=>'ASC','FileUpload.id'=>'ASC')));
		// $file_uploads = $this->FileUpload->find('all');
		$this->set(compact('file_uploads'));
	}
}